package pl.bigosmaciej.mycontrol.apigateway.secutity;

import io.jsonwebtoken.Claims;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.server.SecurityWebFilterChain;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.stream.Collectors;

@Configuration
@EnableWebSecurity
public class Config {

    @Bean
    public SecurityWebFilterChain securityWebFilterChain(ServerHttpSecurity httpSecurity) {
        httpSecurity.csrf(ServerHttpSecurity.CsrfSpec::disable)
                .authorizeExchange(exchange -> exchange.pathMatchers("/user/register").permitAll())
                .authorizeExchange(exchange -> exchange.pathMatchers("/login").permitAll())
                .authorizeExchange(exchange -> exchange.pathMatchers("/actuator/**").permitAll())
                .authorizeExchange(exchange -> exchange.anyExchange().authenticated())
                .oauth2ResourceServer(oauth2 -> oauth2
                        .jwt(jwt -> jwt.authenticationManager(jwtAuthenticationManager())));

        return httpSecurity.build();
    }

    @Bean
    public ReactiveAuthenticationManager jwtAuthenticationManager() {
        return authentication -> {
            String token = authentication.getPrincipal().toString();

            Claims claims = JwtUtils.parseToken(token);

            if (!claims.isEmpty()) {
                List<String> roles = claims.get("roles",List.class);

                List<GrantedAuthority> authorities = roles.stream()
                        .map(SimpleGrantedAuthority::new)
                        .collect(Collectors.toList());
                Authentication authenticatedUser = new UsernamePasswordAuthenticationToken(token, token, authorities);
                return Mono.just(authenticatedUser);
            } else {
                return Mono.error(new UsernameNotFoundException("test"));
            }
        };
    }

}

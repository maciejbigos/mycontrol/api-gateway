package pl.bigosmaciej.mycontrol.apigateway.secutity;

import io.jsonwebtoken.Claims;

import io.jsonwebtoken.Jwts;

public class JwtUtils {

    private static final String SECRET = "TOTALLY NOT IMPORTANT STRING";

    public static Claims parseToken(String token) {
        try {
            return Jwts.parser().setSigningKey(SECRET).parseClaimsJws(token).getBody();
        } catch (Exception e) {
            // W przypadku błędu, token jest nieważny
            return Jwts.claims();
        }
    }
}

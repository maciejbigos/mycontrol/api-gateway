package pl.bigosmaciej.mycontrol.apigateway.filter;

import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.annotation.Order;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import pl.bigosmaciej.mycontrol.apigateway.secutity.JwtUtils;
import reactor.core.publisher.Mono;

@Component
@Order(-1)
public class UserIdFilter implements GlobalFilter {


    public static final String AUTHORIZATION = "Authorization";
    public static final String HEADER_NAME = "X-User-Id-Y";

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        String token = request.getHeaders().getFirst(AUTHORIZATION);
        if (token != null)
            token = token.substring(7);
        String userId = getUserId(token);

        ServerHttpRequest modifiedRequest = request.mutate()
                .header(HEADER_NAME,userId)
                .build();

        return chain.filter(exchange.mutate().request(modifiedRequest).build());
    }

    private String getUserId(String token) {
        return (String) JwtUtils.parseToken(token).get("id");
    }
}

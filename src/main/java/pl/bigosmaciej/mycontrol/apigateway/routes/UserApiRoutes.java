package pl.bigosmaciej.mycontrol.apigateway.routes;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UserApiRoutes {

    @Value("${services.user_service.uri}")
    private String userServiceUri;

    @Value("${services.auth_service.uri}")
    private String authServiceUri;

    @Bean
    public RouteLocator userRoutes(RouteLocatorBuilder builder) {
        return builder.routes()
                .route(p -> p
                        .path("/user/**")
                        .filters(gatewayFilterSpec -> gatewayFilterSpec.rewritePath("/user/(?<path>.*)", "/api/${path}"))
                        .uri(userServiceUri))
                .route(p -> p
                        .path("/login")
                        .uri(authServiceUri))
                .build();
    }

}
